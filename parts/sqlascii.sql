INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    select array_agg( datname ORDER BY datname ) as a from pg_database d where not d.datistemplate and pg_catalog.pg_encoding_to_char(d.encoding) = 'SQL_ASCII'
)
select 'Don''t use SQL_ASCII', 'Don.27t_use_SQL_ASCII', format('You have %s database(s) that violate this rule:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
