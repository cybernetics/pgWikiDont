-- Version header for report
SELECT ' pgWikiDont version 0.2 ';

-- Get count of violations
select count(*) as count from :"ttresults" \gset tt

-- Display congratulations if there are no violations.
with m as (
    select 'Looks that your database is clean. Congratulations.'::text as t
)
SELECT format(E'\n%s\n%s\n', t, repeat('=', length(t))) FROM m WHERE :ttcount = 0;

-- Display header for violations, if there are any
with m as (
    SELECT format( E'Your database violates %s rule(s):', :ttcount ) as t
)
SELECT format(E'\n%s\n%s\n', t, repeat('=', length(t))) FROM m WHERE :ttcount > 0;

-- Display details of violations, if there are any
SELECT
    format(
        E'Rule: %s\n  details: https://wiki.postgresql.org/wiki/Don''t_Do_This#%s\n  %s\n      - %s\n',
        codename,
        url,
        message,
        array_to_string( objects[array_lower(objects, 1):array_lower(objects,1)+9] , E'\n      - ')
    )
FROM :"ttresults" WHERE :ttcount > 0 ORDER BY ordno;

-- Show warning about missing pg_stat_statements, if it's missing.
WITH m as (
    SELECT ' WARNINGS: '::TEXT as t
)
SELECT format(E'\n%s\n%s', t, repeat('=', length(t))) FROM m WHERE EXISTS (SELECT * FROM :"ttwarnings");
SELECT format('- %s', message) FROM :"ttwarnings" WHERE EXISTS (SELECT * FROM :"ttwarnings") ORDER BY ordno;
SELECT '' WHERE EXISTS (SELECT * FROM :"ttwarnings");

-- Drop created temp relations
DROP VIEW :"ttqueries";
DROP TABLE :"ttwarnings";
DROP TABLE :"ttresults";

ROLLBACK;
