INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( c.oid::regclass::text ORDER BY c.oid::regclass::text ) as a
    FROM pg_catalog.pg_class c WHERE c.relkind = 'r' AND c.relname ~ '[A-Z]'
)
select 'Don''t use upper case table names', 'Don.27t_use_upper_case_table_or_column_names', format('You have %s table(s) that contain upper case letters in name:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;

INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( format( '%s.%I', c.oid::regclass::text, a.attname) ORDER BY c.oid::regclass::text, a.attname ) as a
    FROM pg_catalog.pg_attribute a join pg_catalog.pg_class c on a.attrelid = c.oid WHERE c.relkind = 'r' AND a.attname ~ '[A-Z]'
)
select 'Don''t use upper case column names', 'Don.27t_use_upper_case_table_or_column_names', format('You have %s column(s) that contain upper case letters in name:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
