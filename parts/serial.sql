INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( format( '%s.%I', c.oid::regclass::text, a.attname) ORDER BY c.oid::regclass::text, a.attname ) as a
    FROM pg_catalog.pg_attribute a
        join pg_catalog.pg_attrdef d on d.adrelid = a.attrelid AND d.adnum = a.attnum AND a.atthasdef
        join pg_class c on a.attrelid = c.oid
    WHERE
        c.relkind = 'r' AND
        a.attnum > 0 AND NOT a.attisdropped AND
        a.atttypid in ('int4'::regtype, 'int8'::regtype) AND
        c.relname <> :'ttresults' AND
        c.relname <> :'ttwarnings' AND
        pg_catalog.pg_get_expr(d.adbin, d.adrelid, true) ~ '^(pg_catalog\.)?nextval\(''[^'']+''::regclass\)$'
)
select 'Don''t use serial', 'Don.27t_use_serial', format('You have %s column(s) that look like they used serial/bigserial datatype:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
        AND current_setting('server_version_num')::INT4 >= 100000 -- Identity is from version 10.0
;
